import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  users = [
    {
      "userId": 1,
      "name": "Alice",  
      "groups":  [1, 2, 3]
    }
  ]
  groups = [
    {
      "userId": 1,
      "name": "Alice",  
      "groups":  ["UI Engineers"]
    }
  ]

  constructor(private dataService: DataService) { }

  ngOnInit() {

    this.dataService.createUserMethod(this.users).subscribe((data: any[])=>{
      console.log(data);
    })  
    this.dataService.addUserToGroup(this.users).subscribe((data: any[])=>{
      console.log(data);
    })  
  }

}