import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  private CreateUser_SERVER = "http://localhost:3000/user/create?name="

  private AddUsertoGroup_SERVER = "http://localhost:3000/user"

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public createUserMethod(data){
    let url = this.CreateUser_SERVER + data.Name;
    return this.httpClient.post(url, data).pipe(catchError(this.handleError));
  } 
  public addUserToGroup(data){
    let url = this.AddUsertoGroup_SERVER + '/' + data.userId + 'addGroup?name=' + data.data.name;
    return this.httpClient.post(url, data).pipe(catchError(this.handleError));
  }
 
  
}