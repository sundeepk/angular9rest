var faker = require('faker');

var database = { users: []};

for (var i = 1; i<= 2; i++) {
  database.users.push({
    userId: i,
    name: faker.commerce.productName(),
    groups: faker.lorem.sentences()
  });
}

console.log(JSON.stringify(database));